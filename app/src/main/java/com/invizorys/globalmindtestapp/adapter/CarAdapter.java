package com.invizorys.globalmindtestapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.invizorys.globalmindtestapp.R;
import com.invizorys.globalmindtestapp.entity.Car;

import java.util.List;

/**
 * Created by Paryshkura Roman on 27.05.2015.
 */
public class CarAdapter extends BaseAdapter {
    private Context context;
    private boolean isUserLoggined;
    private List<Car> cars;

    public CarAdapter(Context context, List<Car> cars, boolean isUserLoggined) {
        this.context = context;
        this.isUserLoggined = isUserLoggined;
        this.cars = cars;
    }

    @Override
    public int getCount() {
        return cars.size();
    }

    @Override
    public Car getItem(int position) {
        return cars.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.expandable_list_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.textViewVehicleType = (TextView) convertView.findViewById(R.id.textview_vehicle_type);
            viewHolder.textViewBrand = (TextView) convertView.findViewById(R.id.textview_brand);
            viewHolder.textViewColor = (TextView) convertView.findViewById(R.id.textview_color);
            viewHolder.textViewCondition = (TextView) convertView.findViewById(R.id.textview_condition);
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textview_name);
            viewHolder.textViewPhoneNumber = (TextView) convertView.findViewById(R.id.textview_phone_number);
            viewHolder.buttonExpandable = (Button) convertView.findViewById(R.id.expandable_toggle_button);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewVehicleType.setText(getItem(position).getVehicle_type());
        viewHolder.textViewBrand.setText(getItem(position).getBrand());
        viewHolder.textViewColor.setText(getItem(position).getColor());
        viewHolder.textViewCondition.setText(getItem(position).getCondition());

        viewHolder.textViewName.setText(context.getString(R.string.name) + "\n" + getItem(position).getOwner().getName());
        viewHolder.textViewPhoneNumber.setText(context.getString(R.string.phone_number) + "\n" + getItem(position).getOwner().getPhone_number());

        if (!isUserLoggined) {
            viewHolder.buttonExpandable.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.buttonExpandable.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView textViewVehicleType;
        TextView textViewBrand;
        TextView textViewColor;
        TextView textViewCondition;
        TextView textViewName;
        TextView textViewPhoneNumber;
        Button buttonExpandable;
    }
}
