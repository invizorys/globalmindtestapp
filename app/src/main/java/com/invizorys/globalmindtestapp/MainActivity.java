package com.invizorys.globalmindtestapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.invizorys.globalmindtestapp.adapter.CarAdapter;
import com.invizorys.globalmindtestapp.entity.Car;
import com.invizorys.globalmindtestapp.view.expandableListView.ActionSlideExpandableListView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private boolean isLoggined = false;
    public static final String IS_LOGGINED = "isLoggined";
    public static final String JSON_FILE_NAME = "cars.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null) {
            isLoggined = savedInstanceState.getBoolean(IS_LOGGINED);
        }

        Type listType = new TypeToken<ArrayList<Car>>() {}.getType();
        final List<Car> cars = new Gson().fromJson(loadJSONFromAssets(), listType);

        final ActionSlideExpandableListView list = (ActionSlideExpandableListView) this.findViewById(R.id.list);
        list.setAdapter(new CarAdapter(this, cars, isLoggined));

        final Button buttonLogin = (Button) findViewById(R.id.button_login);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLoggined) {
                    isLoggined = true;
                    buttonLogin.setText(getString(R.string.logout));
                    list.setAdapter(new CarAdapter(MainActivity.this, cars, isLoggined));
                } else {
                    isLoggined = false;
                    buttonLogin.setText(getString(R.string.login));
                    list.setAdapter(new CarAdapter(MainActivity.this, cars, isLoggined));
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_LOGGINED, isLoggined);
    }

    private String loadJSONFromAssets() {
        String json;
        try {
            InputStream is = getAssets().open(JSON_FILE_NAME);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
